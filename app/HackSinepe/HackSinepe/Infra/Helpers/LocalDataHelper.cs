﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Storage;

namespace HackSinepe.Infra.Helpers
{
    public static class LocalDataHelper
    {
        private const string EVALUATIONS_DATA = "evaluations.txt";
        private const string BACKGROUND_TASK_EXECUTION = "background_task_execution";

        public static bool GetBackgroundTaskExecution()
        {
            var localsettings = ApplicationData.Current.LocalSettings;

            var value = localsettings.Values[BACKGROUND_TASK_EXECUTION] as string;
            var isExecution = false;

            bool.TryParse(value, out isExecution);

            return isExecution;
        }

        public static bool SetBackgroundTaskExecution(bool isExecution)
        {
            var localsettings = ApplicationData.Current.LocalSettings;

            localsettings.Values[BACKGROUND_TASK_EXECUTION] = isExecution.ToString();
            
            return isExecution;
        }

        //public static async Task SaveEvaluationData(Evaluation data)
        //{
        //    try
        //    {
        //        var evaluations = await LoadEvaluationData();

        //        if (evaluations == null)
        //            evaluations = new List<Evaluation>();

        //        evaluations.Add(data);

        //        StorageFile file = await ApplicationData.Current.LocalFolder
        //        .CreateFileAsync(EVALUATIONS_DATA, CreationCollisionOption.ReplaceExisting);

        //        var dataSerialized = JsonConvert.SerializeObject(evaluations);

        //        await FileIO.WriteTextAsync(file, dataSerialized);
        //    }
        //    catch (Exception)
        //    {
                
        //    }
        //}

        //public static async Task RemoveEvaluationData(string id)
        //{
        //    var evaluations = await LoadEvaluationData();

        //    evaluations.RemoveAll(e => e.Id == id);

        //    StorageFile file = await ApplicationData.Current.LocalFolder
        //    .CreateFileAsync(EVALUATIONS_DATA, CreationCollisionOption.ReplaceExisting);

        //    var dataSerialized = JsonConvert.SerializeObject(evaluations);

        //    await FileIO.WriteTextAsync(file, dataSerialized);
        //}

        //public static async Task<List<Evaluation>> LoadEvaluationData()
        //{
        //    try
        //    {
        //        var file = await ApplicationData.Current.LocalFolder.GetFileAsync(EVALUATIONS_DATA);
                
        //        var dataSerialized = await FileIO.ReadTextAsync(file);

        //        Debug.WriteLine("LocalDataHelper: " + dataSerialized);

        //        return JsonConvert.DeserializeObject<List<Evaluation>>(dataSerialized) as List<Evaluation>;
        //    }
        //    catch (Exception)
        //    {
        //        return null;
        //    }
        //}
    }
}