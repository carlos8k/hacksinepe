﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace HackSinepe.Infra.Helpers
{
    public static class ApiHelper
    {
        public const string URL_BASE = "https://hacksinepe.azurewebsites.net/";
        public static string ApiToken;
        public static string FacebookToken;

        public async static Task<T> Post<T>(string endpoint, List<KeyValuePair<string, string>> parameters, bool isAuthRequired = true)
        {
            using (var handler = new HttpClientHandler { AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(URL_BASE);

                if (isAuthRequired || ApiToken != null)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", ApiToken);

                var result = await client.PostAsync(endpoint, new FormUrlEncodedContent(parameters));

                if (result.IsSuccessStatusCode)
                {
                    var json = await result.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(json);
                }

                var contentEx = await result.Content.ReadAsStringAsync();
                throw new InvalidOperationException(contentEx);
            }
        }

        public async static Task<T> Post<T>(string endpoint, object body, bool isAuthRequired = true)
        {
            using (var handler = new HttpClientHandler { AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(URL_BASE);

                if (isAuthRequired || ApiToken != null)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", ApiToken);

                var postBody = body != null ? JsonConvert.SerializeObject(body) : "";

                var result = await client.PostAsync(endpoint, new StringContent(postBody, Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    var content = await result.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(content);
                }

                if (result.StatusCode == HttpStatusCode.Unauthorized)
                    throw new UnauthorizedAccessException();

                var contentEx = await result.Content.ReadAsStringAsync();
                throw new InvalidOperationException(contentEx);
            }
        }

        public async static Task Put(string endpoint, bool isAuthRequired = true)
        {
            using (var handler = new HttpClientHandler { AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(URL_BASE);

                if (isAuthRequired || ApiToken != null)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", ApiToken);

                var result = await client.PutAsync(endpoint, null);

                if (result.IsSuccessStatusCode)
                {
                    return;
                }

                var content = await result.Content.ReadAsStringAsync();
                throw new InvalidOperationException(content);
            }
        }

        public async static Task Patch(string endpoint, object body, bool isAuthRequired = true)
        {
            using (var handler = new HttpClientHandler { AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(URL_BASE);

                if (isAuthRequired || ApiToken != null)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", ApiToken);

                var postBody = JsonConvert.SerializeObject(body);

                var result = await client.PatchCustomAsync(endpoint, new StringContent(postBody, Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return;
                }

                var content = await result.Content.ReadAsStringAsync();
                throw new InvalidOperationException(content);
            }
        }

        public async static Task<T> Get<T>(string endpoint, bool isAuthRequired = true)
        {
            using (var handler = new HttpClientHandler { AllowAutoRedirect = false })
            using (var client = new HttpClient(handler))
            {
                client.BaseAddress = new Uri(URL_BASE);

                if (isAuthRequired || ApiToken != null)
                    client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", ApiToken);

                var result = await client.GetAsync(endpoint);

                if (result.IsSuccessStatusCode)
                {
                    var json = await result.Content.ReadAsStringAsync();

                    return JsonConvert.DeserializeObject<T>(json);
                }

                var content = await result.Content.ReadAsStringAsync();
                throw new InvalidOperationException(content);
            }
        }
    }

    public static class HttpClientExtensions
    {
        public static async Task<HttpResponseMessage> PatchCustomAsync(this HttpClient client, string endpoint, HttpContent iContent)
        {
            var method = new System.Net.Http.HttpMethod("PATCH");
            var request = new HttpRequestMessage(method, endpoint)
            {
                Content = iContent
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (TaskCanceledException e)
            {
                throw e;
            }

            return response;
        }

        public static async Task<HttpResponseMessage> PutCustomAsync(this HttpClient client, string endpoint, HttpContent iContent)
        {
            var method = new HttpMethod("PUT");
            var request = new HttpRequestMessage(method, endpoint)
            {
                Content = iContent
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (TaskCanceledException e)
            {
                throw e;
            }

            return response;
        }

        public static async Task<HttpResponseMessage> PostCustomAsync(this HttpClient client, string endpoint, HttpContent iContent)
        {
            var method = new HttpMethod("POST");

            var request = new HttpRequestMessage(method, endpoint)
            {
                Content = iContent
            };

            HttpResponseMessage response = new HttpResponseMessage();
            try
            {
                response = await client.SendAsync(request);
            }
            catch (TaskCanceledException e)
            {
                throw e;
            }

            return response;
        }
    }
}