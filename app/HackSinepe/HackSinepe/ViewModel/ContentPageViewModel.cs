﻿using HackSinepe.Models;
using System.Collections.Generic;

namespace HackSinepe.ViewModel
{
    public class ContentPageViewModel
    {
        public IEnumerable<Content> Contents { get; set; }
        public Category Category { get; set; }
        public Student Student { get; set; }
        public string LikesCount { get; set; }
    }
}