﻿using HackSinepe.Models;

namespace HackSinepe.ViewModel
{
    public class CategoryPageViewModel
    {
        public Student Student { get; set; }
        public string LikesCount { get; set; }
    }
}