﻿namespace HackSinepe.Models
{
    public class Content
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public Category Category { get; set; }
        public int CategoryId { get; set; }
        public bool Liked { get; set; }
    }
}