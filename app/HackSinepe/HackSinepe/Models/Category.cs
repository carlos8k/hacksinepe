﻿namespace HackSinepe.Models
{
    public class Category
    {
        public int Id { get; set; } = 1;
        public string Name { get; set; } = "Teste";
        public string Color { get; set; } = "Red";
        public string ImageUrl { get; set; } = "ms-appx:///Assets/LockScreenLogo.scale-200.png";
    }
}