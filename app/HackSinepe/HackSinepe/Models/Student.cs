﻿namespace HackSinepe.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ImageUrl { get; set; }
        public string Class { get; set; }
    }
}