﻿namespace HackSinepe.Models
{
    public class Login
    {
        public string Access_token { get; set; }
        public string UserId { get; set; }
        public string Type { get; set; }
    }
}