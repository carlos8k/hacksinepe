﻿namespace HackSinepe.Models
{
    public class AuthInfo
    {
        public string Token { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}