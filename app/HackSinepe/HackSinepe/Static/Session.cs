﻿using HackSinepe.Models;
using System.Collections.Generic;

namespace HackSinepe.Static
{
    public static class Session
    {
        public static List<Content> Contents { get; set; } = new List<Content>();
        public static Category CurrentCategory { get; set; }
        public static AuthInfo AuthInfo { get; internal set; }
        public static int LikesCount { get; set; }
    }
}
