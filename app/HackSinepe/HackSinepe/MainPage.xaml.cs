﻿using HackSinepe.Models;
using HackSinepe.Pages;
using HackSinepe.Static;
using HackSinepe.ViewModel;
using System.Collections.Generic;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace HackSinepe
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public List<Category> Categories { get; set; } = new List<Category>
        {
            new Category{ Id = 1, ImageUrl = "ms-appx:///Assets/img/GAME.png", Name = "GAME", Color = "#E84A3C" },
            new Category{ Id = 2, ImageUrl = "ms-appx:///Assets/img/ARTES.png", Name = "ARTES", Color = "#95C11E" },
            new Category{ Id = 3, ImageUrl = "ms-appx:///Assets/img/FILMES.png", Name = "FILMES", Color = "#F59D17" },
            new Category{ Id = 4, ImageUrl = "ms-appx:///Assets/img/MUSICAS.png", Name = "MUSICAS", Color = "#2C2E83" },
            new Category{ Id = 5, ImageUrl = "ms-appx:///Assets/img/LIVROS.png", Name = "LIVROS", Color = "#905AA1" },
        };

        public CategoryPageViewModel Model { get; set; } = new CategoryPageViewModel
        {
            Student = new Student
            {
                Class = "TURMA: 6B",
                ImageUrl = "ms-appx:///Assets/img/MULEK.png",
                Name = "Carlos Balsalobre"
            }
        };

        public MainPage()
        {
            InitializeComponent();

            Model.LikesCount = $"{Session.LikesCount} likes";

            CategoriesListView.ItemsSource = Categories;
        }

        private void CategoriesListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var category = e.ClickedItem as Category;

                if (category != null)
                {
                    Session.CurrentCategory = category;
                    Frame.Navigate(typeof(ContentPage));
                }
            }
            catch
            {
            }
        }
    }
}