﻿using HackSinepe.Infra.Helpers;
using HackSinepe.Models;
using HackSinepe.Static;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HackSinepe.Services
{
    public class AuthService
    {
        public bool IsLoggedIn()
        {
            var authInfo = Session.AuthInfo;

            if (authInfo != null &&
                authInfo?.Password != null &&
                authInfo?.UserName != null &&
                authInfo.Token != null)
            {
                return true;
            }

            return false;
        }

        public async Task LoginAsync(string userName, string password)
        {
            var authInfo = new AuthInfo
            {
                UserName = userName,
                Password = password
            };

            var parameters = new List<KeyValuePair<string, string>>();

            parameters.Add(new KeyValuePair<string, string>("client_id", "hacksinepe"));
            parameters.Add(new KeyValuePair<string, string>("client_secret", "eyJhbGcJhGTSUzI1NiIsImtpZCI6IjBiZTg5Y"));

            parameters.Add(new KeyValuePair<string, string>("grant_type", "password"));
            parameters.Add(new KeyValuePair<string, string>("scope", "hacksinepe"));

            parameters.Add(new KeyValuePair<string, string>("username", userName));
            parameters.Add(new KeyValuePair<string, string>("password", password));

            try
            {
                var loginModel = await ApiHelper.Post<Login>(API.TOKEN_POST, parameters, false);

                if (loginModel != null && loginModel?.Access_token != null)
                {
                    authInfo.Token = loginModel.Access_token;
                }
                else
                {
                    throw new UnauthorizedAccessException("Usuário ou senha inválido.");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            Session.AuthInfo = authInfo;
            Session.LikesCount = 3;
        }
    }
}