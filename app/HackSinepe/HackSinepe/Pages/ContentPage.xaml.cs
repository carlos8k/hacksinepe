﻿using HackSinepe.Infra.Helpers;
using HackSinepe.Models;
using HackSinepe.Static;
using HackSinepe.ViewModel;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.UI.Popups;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;

namespace HackSinepe.Pages
{
    public sealed partial class ContentPage : Page
    {
        public ContentPageViewModel Model { get; set; } = new ContentPageViewModel();

        public ContentPage()
        {
            InitializeComponent();

            Model.Category = Session.CurrentCategory;

            Model.Student = new Student
            {
                Class = "TURMA: 6B",
                ImageUrl = "ms-appx:///Assets/img/MULEK.png",
                Name = "Carlos Balsalobre"
            };

            Model.LikesCount = $"{Session.LikesCount} likes";
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            try
            {
                base.OnNavigatedTo(e);

                await GetContentAsync();
            }
            catch 
            {
            }
            
        }

        private async Task GetContentAsync()
        {
            try
            {
                Model.Contents = Session.Contents.Where(c => c.CategoryId == Session.CurrentCategory.Id);

                if (Model.Contents == null || Model.Contents.Count() == 0)
                {
                    Model.Contents = await ApiHelper.Get<IEnumerable<Models.Content>>($"api/contents/{Session.CurrentCategory.Id}", false);
                    Session.Contents.AddRange(Model.Contents);
                }

                ContentsListView.ItemsSource = Model.Contents;
            }
            catch
            {
            }
            
        }


        private async void ContentsListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            try
            {
                var content = e.ClickedItem as Content;

                if (content != null)
                {
                    await CreateLikeAsync(content.Id);

                    Session.LikesCount--;
                    content.Liked = true;

                    if (Session.LikesCount == 0)
                        Frame.Navigate(typeof(LoginPage));
                    else
                        Frame.Navigate(typeof(MainPage));
                }
            }
            catch
            {
            }
            
        }

        private async Task CreateLikeAsync(int contentId)
        {
            try
            {
                ApiHelper.ApiToken = Session.AuthInfo.Token;
                await ApiHelper.Post<object>(endpoint: $"api/likes/{contentId}", body: null, isAuthRequired: true);
            }
            catch
            {
            }
        }
    }
}