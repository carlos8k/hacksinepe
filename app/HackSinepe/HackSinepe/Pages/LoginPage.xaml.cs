﻿using HackSinepe.Services;
using HackSinepe.Static;
using System;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;

namespace HackSinepe.Pages
{
    public sealed partial class LoginPage : Page
    {
        AuthService _authService;

        public LoginPage()
        {
            InitializeComponent();

            _authService = new AuthService();
        }

        private void PinPass1_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            PinPass2.Focus(FocusState.Programmatic);
        }

        private void PinPass2_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            PinPass3.Focus(FocusState.Programmatic);
        }

        private void PinPass3_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            PinPass4.Focus(FocusState.Programmatic);
        }

        private async void PinPass4_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            try
            {
                progressSend.Visibility = Visibility.Visible;

                var password = $"{PinPass1.Password}{PinPass2.Password}{PinPass3.Password}{PinPass4.Password}";
                await _authService.LoginAsync($"welike{password}@we.com", password);

                Frame.Navigate(typeof(MainPage));
            }
            catch
            {
                PinPass1.Password = string.Empty;
                PinPass2.Password = string.Empty;
                PinPass3.Password = string.Empty;
                PinPass4.Password = string.Empty;

                PinPass1.Focus(FocusState.Programmatic);

                tbError.Text = "PIN inválido";
                tbError.Visibility = Visibility.Visible;
                spMessage.Visibility = Visibility.Collapsed;
                progressSend.Visibility = Visibility.Collapsed;
            }
        }
    }
}