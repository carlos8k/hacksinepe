﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Interfaces.Custom;
using Neostore.Backend.Domain.Interfaces.Repository;
using Neostore.Backend.Domain.Models.Custom;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Custom;
using System.Threading.Tasks;

namespace Neostore.Backend.Business.Custom
{
    public class EvaluationService : IEvaluationService
    {
        IMapper _mapper;
        IRepository<Buyer> _buyerRepository;
        IRepository<Evaluation> _evaluationRepository;
        IRepository<Seller> _sellerRepository;
        IRepository<EvaluationReason> _evaluationReasonRepository;
        IRepository<Reason> _reasonRepository;

        public EvaluationService(IMapper mapper,
            IRepository<Buyer> buyerRepository,
            IRepository<Evaluation> evaluationRepository,
            IRepository<Seller> sellerRepository,
            IRepository<EvaluationReason> evaluationReasonRepository,
            IRepository<Reason> reasonRepository)
        {
            _mapper = mapper;
            _buyerRepository = buyerRepository;
            _evaluationRepository = evaluationRepository;
            _sellerRepository = sellerRepository;
            _evaluationReasonRepository = evaluationReasonRepository;
            _reasonRepository = reasonRepository;
        }

        public async Task<EvaluationViewModel> CreateAsync(string userId, EvaluationViewModel model)
        {
            var evaluation = _mapper.Map<Evaluation>(model);

            // the logged in user is the seller
            evaluation.Seller = await _sellerRepository.List
                .FirstOrDefaultAsync(s => s.Id == userId);

            // if buyer was identified, set in the evaluation
            if(model.Buyer != null)
            {
                evaluation.Buyer = await _buyerRepository.List
                .FirstOrDefaultAsync(s => s.Id == model.Buyer.Id);
            }

            // iterate all reasons selected
            foreach (var item in model.Reasons)
            {
                var evaluationReason = new EvaluationReason
                {
                    Evaluation = evaluation,
                    Reason = await _reasonRepository.List.FirstOrDefaultAsync(r => r.Id == item.Id)
                };

                evaluation.EvaluationReasons.Add(evaluationReason);
            }

            _evaluationRepository.Insert(evaluation);

            await _evaluationRepository.SaveChangesAsync();

            return _mapper.Map<EvaluationViewModel>(evaluation);
        }
    }
}