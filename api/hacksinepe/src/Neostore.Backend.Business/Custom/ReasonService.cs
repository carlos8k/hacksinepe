﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Interfaces.Custom;
using Neostore.Backend.Domain.Interfaces.Repository;
using Neostore.Backend.Domain.Models.Custom;
using Neostore.Backend.Domain.ViewModels.Custom;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.Business.Custom
{
    public class ReasonService : IReasonService
    {
        IMapper _mapper;
        IRepository<Reason> _reasonRepository;

        public ReasonService(IMapper mapper,
            IRepository<Reason> reasonRepository)
        {
            _mapper = mapper;
            _reasonRepository = reasonRepository;
        }

        public async Task<ReasonViewModel> CreateAsync(ReasonViewModel model)
        {
            var reason = _mapper.Map<Reason>(model);

            _reasonRepository.Insert(reason);
            await _reasonRepository.SaveChangesAsync();

            return _mapper.Map<ReasonViewModel>(reason);
        }

        public async Task DeleteAsync(int id)
        {
            var reason = await _reasonRepository.List
                .FirstOrDefaultAsync(r => r.Id == id);

            reason.IsDeleted = true;

            _reasonRepository.Update(reason);
            await _reasonRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<ReasonViewModel>> GetAsync()
        {
            var reasons = await _reasonRepository.List
                .Where(r => !r.IsDeleted)
                .ToListAsync();

            return reasons.Select(r => _mapper.Map<ReasonViewModel>(r));
        }

        public async Task<ReasonViewModel> GetAsync(int id)
        {
            var reason = await _reasonRepository.List
                .FirstOrDefaultAsync(r => r.Id == id);

            return _mapper.Map<ReasonViewModel>(reason);
        }

        public async Task UpdateAsync(int id, ReasonViewModel model)
        {
            var reason = await _reasonRepository.List
                .FirstOrDefaultAsync(r => r.Id == id);

            reason = _mapper.Map(model, reason, typeof(ReasonViewModel), typeof(Reason)) as Reason;

            _reasonRepository.Update(reason);
            await _reasonRepository.SaveChangesAsync();
        }
    }
}