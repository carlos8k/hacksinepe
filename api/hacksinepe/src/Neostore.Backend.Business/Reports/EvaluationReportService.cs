﻿using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Models.Context;
using Neostore.Backend.Domain.Models.Users;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.Business.Reports
{
    public class EvaluationReportService
    {
        BackendContext _context;

        public EvaluationReportService(BackendContext context)
        {
            _context = context;
        }

        public async Task<decimal> GetEvaluationsAverage()
        {
            var evaluationsRatingSum = await _context.Evaluations.SumAsync(e => e.Rating);
            var evaluationCount = await GetEvaluationsCount();

            if (evaluationCount == 0)
                return 0;

            var evaluationAverage = evaluationsRatingSum / (decimal)evaluationCount;

            return evaluationAverage;
        }

        public async Task<int> GetEvaluationsCount()
        {
            var evaluationCount = await _context.Evaluations.CountAsync();

            return evaluationCount;
        }

        public async Task<int> GetEvaluationsCountAverage()
        {
            var evaluationCount = await GetEvaluationsCount();
            var sellerCount = await _context.Users.OfType<Seller>().CountAsync();

            if (sellerCount == 0)
                return 0;

            return evaluationCount / sellerCount;
        }

        public async Task<int> GetEvaluationsCountMax()
        {
            var sellerListCount = new Dictionary<string, int>();
            var sellers = await _context.Users.OfType<Seller>().ToListAsync();
            
            foreach (var seller in sellers)
            {
                var count = await _context.Evaluations.CountAsync(e => e.Seller.Id == seller.Id);
                sellerListCount.Add(seller.Id, count);
            }

            var maxEvaluation = 0;

            foreach (var sellerCount in sellerListCount)
            {
                var count = sellerCount.Value;

                if(count > maxEvaluation)
                {
                    maxEvaluation = count;
                }
            }

            return maxEvaluation;
        }

        public async Task<int> GetEvaluationsCountMin()
        {
            var sellerListCount = new Dictionary<string, int>();
            var sellers = await _context.Users.OfType<Seller>().ToListAsync();

            foreach (var seller in sellers)
            {
                var count = await _context.Evaluations.CountAsync(e => e.Seller.Id == seller.Id);
                sellerListCount.Add(seller.Id, count);
            }

            var minEvaluation = int.MaxValue;

            foreach (var sellerCount in sellerListCount)
            {
                var count = sellerCount.Value;

                if (count < minEvaluation)
                {
                    minEvaluation = count;
                }
            }

            return minEvaluation;
        }

        public async Task<IEnumerable<double>> GetEvaluationPercents()
        {
            var ratingOneCount = await _context.Evaluations.CountAsync(e => e.Rating == 1);
            var ratingTwoCount = await _context.Evaluations.CountAsync(e => e.Rating == 2);
            var ratingThreeCount = await _context.Evaluations.CountAsync(e => e.Rating == 3);
            var ratingFourCount = await _context.Evaluations.CountAsync(e => e.Rating == 4);
            var ratingFiveCount = await _context.Evaluations.CountAsync(e => e.Rating == 5);

            var evaluationCount = await GetEvaluationsCount();

            if (evaluationCount == 0)
                return new[] { (double)0, 0, 0, 0, 0 };

            var evaluationPercents = new[]
            {
                ((double)ratingOneCount / evaluationCount * 100),
                ((double)ratingTwoCount / evaluationCount * 100),
                ((double)ratingThreeCount / evaluationCount * 100),
                ((double)ratingFourCount / evaluationCount * 100),
                ((double)ratingFiveCount / evaluationCount * 100),
            };

            return evaluationPercents;
        }
    }
}