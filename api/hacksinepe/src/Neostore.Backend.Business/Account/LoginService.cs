﻿using Microsoft.AspNetCore.Identity;
using Neostore.Backend.Domain.Interfaces.Services.Account;
using Neostore.Backend.Domain.Models.Users;
using System;
using System.Threading.Tasks;

namespace Neostore.Backend.Business.Account
{
    public class LoginService : ILoginService
    {
        SignInManager<UserBase> signInManager;

        public LoginService(SignInManager<UserBase> _signInManager)
        {
            signInManager = _signInManager;
        }

        public async Task SignInAsync(string username, string password)
        {
            var result = await signInManager.PasswordSignInAsync(username, password, true, false);
            if(!result.Succeeded)
                throw new InvalidOperationException("Username or password inválid");
        }

        public async Task SignOutAsync()
        {
            await signInManager.SignOutAsync();
        }
    }
}