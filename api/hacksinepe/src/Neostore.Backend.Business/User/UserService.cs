﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Interfaces.Repository;
using Neostore.Backend.Domain.Interfaces.Services.User;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Users;
using System;
using System.Threading.Tasks;

namespace Neostore.Backend.Business.User
{
    public class UserService : IUserService
    {
        IMapper mapper;
        IRepository<UserBase> userRepository;

        public UserService(IRepository<UserBase> _userRepository, IMapper _mapper)
        {
            userRepository = _userRepository;
            mapper = _mapper;
        }

        public async Task<UserBaseViewModel> GetAsync(string id)
        {
            var user = await userRepository.List
                .FirstOrDefaultAsync(u => u.Id == id);

            return mapper.Map<UserBaseViewModel>(user);
        }

        public async Task LogoutAsync(string userId)
        {
            var user = await userRepository.List
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null) throw new InvalidOperationException("User not found");

            user.DeviceToken = null;
            userRepository.Update(user);
            await userRepository.SaveChangesAsync();
        }

        public async Task UpdateDeviceTokenAsync(string deviceToken, string userId)
        {
            var user = await userRepository.List
                .FirstOrDefaultAsync(u => u.Id == userId);

            if (user == null) throw new InvalidOperationException("User not found");
            if (string.IsNullOrWhiteSpace(deviceToken)) throw new InvalidOperationException("Device token is invalid");

            user.DeviceToken = deviceToken;
            userRepository.Update(user);
            await userRepository.SaveChangesAsync();
        }
    }
}