﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Interfaces.Repository;
using Neostore.Backend.Domain.Interfaces.Services.User;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.Business.User
{
    public class SellerRegisterService : IRegisterService<Seller, SellerViewModel>
    {
        IMapper _mapper;
        UserManager<UserBase> _userManager;
        IRepository<Seller> _sellerRepository;

        public SellerRegisterService(IMapper mapper,
            UserManager<UserBase> userManager,
            IRepository<Seller> sellerRepository)
        {
            _mapper = mapper;
            _userManager = userManager;
            _sellerRepository = sellerRepository;
        }

        public async Task<SellerViewModel> CreateAsync(SellerViewModel model)
        {
            var seller = _mapper.Map<Seller>(model);

            seller.UserName = seller.Email;

            var resultUser = await _userManager.CreateAsync(seller, model.Password);
            var resultRole = await _userManager.AddToRoleAsync(seller, nameof(Seller));

            if(!resultUser.Succeeded)
                throw new InvalidOperationException(resultUser.Errors.ToString());
            if (!resultRole.Succeeded)
                throw new InvalidOperationException(resultRole.Errors.ToString());


            return _mapper.Map<SellerViewModel>(seller);
        }

        public async Task DeleteAsync(string id)
        {
            var seller = await _sellerRepository.List
                .FirstOrDefaultAsync(s => s.Id == id);

            seller.IsDeleted = true;

            _sellerRepository.Update(seller);
            await _sellerRepository.SaveChangesAsync();
        }

        public async Task<IEnumerable<SellerViewModel>> GetAsync()
        {
            var sellers = await _sellerRepository.List
                .Where(s => !s.IsDeleted)
                .ToListAsync();

            return sellers.Select(s => _mapper.Map<SellerViewModel>(s));
        }

        public async Task<SellerViewModel> GetAsync(string id)
        {
            var seller = await _sellerRepository.List
                .FirstOrDefaultAsync(s => s.Id == id);

            return _mapper.Map<SellerViewModel>(seller);
        }

        public async Task UpdateAsync(string id, SellerViewModel model)
        {
            var passwordHash = new PasswordHasher<UserBase>();

            var seller = await _sellerRepository.List
                .FirstOrDefaultAsync(s => s.Id == id);

            seller = _mapper.Map(model, seller, typeof(SellerViewModel), typeof(Seller)) as Seller;

            seller.UserName = seller.Email;

            seller.NormalizedEmail = seller.Email.ToUpper();
            seller.NormalizedUserName = seller.UserName.ToUpper();
            seller.SecurityStamp = Guid.NewGuid().ToString();
            seller.CreateDate = DateTime.UtcNow;

            if (model.Password != null)
                seller.PasswordHash = passwordHash.HashPassword(seller, model.Password);

            _sellerRepository.Update(seller);
            await _sellerRepository.SaveChangesAsync();
        }
    }
}