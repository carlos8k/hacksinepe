﻿using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using static IdentityServer4.IdentityServerConstants;

namespace Neostore.Backend.Infrastructure.Config
{
    public class IdentityServerConfig
    {
        // scopes define the resources in your system
        public static IEnumerable<Scope> GetScopes(IConfigurationRoot configuration)
        {
            return new List<Scope>
            {
                new Scope
                {
                    Name = configuration["Application:Scope"],
                    DisplayName = configuration["Application:Name"],
                    Description = configuration["Application:Description"]
                }
            };
        }

        // client want to access resources (aka scopes)
        public static IEnumerable<Client> GetClients(IConfigurationRoot configuration)
        {
            // client credentials client
            return new List<Client>
            {
                new Client
                {
                    ClientName = "Teste",
                    ClientId = "client",
                    AllowedGrantTypes = GrantTypes.ClientCredentials,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { configuration["Application:Scope"] }
                },

                // resource owner password grant client
                new Client
                {
                    ClientName = "Teste",
                    ClientId = "ro.client",
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },
                    AllowedScopes = { configuration["Application:Scope"] }
                },

                // OpenID Connect hybrid flow and client credentials client (MVC)
                new Client
                {
                    ClientId = "mvc",
                    ClientName = "MVC Client",
                    AllowedGrantTypes = GrantTypes.HybridAndClientCredentials,

                    RequireConsent = true,

                    ClientSecrets =
                    {
                        new Secret("secret".Sha256())
                    },

                    RedirectUris = { "http://localhost:4242/signin-oidc" },
                    PostLogoutRedirectUris = { "http://localhost:4242" },

                    AllowedScopes =
                    {
                        StandardScopes.OpenId,
                        StandardScopes.Profile,
                        StandardScopes.OfflineAccess,
                        configuration["Application:Scope"]
                    }
                }
            };
        }
    }
}