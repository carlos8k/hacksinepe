﻿using AutoMapper;
using Neostore.Backend.Domain.Models.Custom;
using Neostore.Backend.Domain.Models.Location;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Custom;
using Neostore.Backend.Domain.ViewModels.Location;
using Neostore.Backend.Domain.ViewModels.Users;
using System;
using System.Linq.Expressions;

namespace Neostore.Backend.Infrastructure.Config
{
    public class AutoMapperConfig
    {
        public static IMapper RegisterMaps()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Address, AddressViewModel>().PreserveReferences();
                cfg.CreateMap<AddressViewModel, Address>().Ignore(v => v.Id).PreserveReferences()
                .ForAllMembers(opt => opt.Condition(srs => !(srs == null)));

                cfg.CreateMap<Evaluation, EvaluationViewModel>().PreserveReferences();
                cfg.CreateMap<EvaluationViewModel, Evaluation>().Ignore(v => v.Id).PreserveReferences()
                .ForAllMembers(opt => opt.Condition(srs => !(srs == null)));

                cfg.CreateMap<Reason, ReasonViewModel>().PreserveReferences();
                cfg.CreateMap<ReasonViewModel, Reason>().Ignore(v => v.Id).PreserveReferences()
                .ForAllMembers(opt => opt.Condition(srs => !(srs == null)));

                cfg.CreateMap<EvaluationReason, EvaluationReasonViewModel>().PreserveReferences();
                cfg.CreateMap<EvaluationReasonViewModel, EvaluationReason>().PreserveReferences()
                .ForAllMembers(opt => opt.Condition(srs => !(srs == null)));

                cfg.CreateMap<Buyer, BuyerViewModel>().PreserveReferences();
                cfg.CreateMap<BuyerViewModel, Buyer>().Ignore(v => v.Id).PreserveReferences()
                .ForAllMembers(opt => opt.Condition(srs => !(srs == null)));

                cfg.CreateMap<Seller, SellerViewModel>().PreserveReferences();
                cfg.CreateMap<SellerViewModel, Seller>().Ignore(v => v.Id).PreserveReferences()
                .ForAllMembers(opt => opt.Condition(srs => !(srs == null)));

                //cfg.CreateMap<IdentityClient, IdentityClientViewModel>().PreserveReferences();
                //cfg.CreateMap<IdentityClientViewModel, IdentityClient>().Ignore(v => v.Id).PreserveReferences()
                //.ForAllMembers(opt => opt.Condition(srs => !(srs == null)));

                cfg.CreateMap<UserBase, UserBaseViewModel>().PreserveReferences();
                cfg.CreateMap<UserBaseViewModel, UserBase>().Ignore(v => v.Id).PreserveReferences()
                .ForAllMembers(opt => opt.Condition(srs => !(srs == null)));
            });

            return config.CreateMapper();
        }
    }

    public static class AutoMapperExtensions
    {
        public static IMappingExpression<TSource, TDestination> Ignore<TSource, TDestination>(this IMappingExpression<TSource, TDestination> map, Expression<Func<TDestination, object>> selector)
        {
            map.ForMember(selector, config => config.Ignore());
            return map;
        }
    }
}