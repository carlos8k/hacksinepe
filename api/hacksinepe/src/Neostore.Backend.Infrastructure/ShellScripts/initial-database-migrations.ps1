dotnet ef migrations add Initial -c BackendContext -o Data/Migrations/Application/BackendContextDb
dotnet ef migrations add Initial -c PersistedGrantDbContext -o Data/Migrations/IdentityServer/PersistedGrantDb
dotnet ef migrations add Initial -c ConfigurationDbContext -o Data/Migrations/IdentityServer/ConfigurationDb

dotnet ef database update -c BackendContext
dotnet ef database update -c PersistedGrantDbContext
dotnet ef database update -c ConfigurationDbContext