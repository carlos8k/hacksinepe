﻿using Neostore.Backend.Domain.Interfaces.Services.User;
using Neostore.Backend.Domain.Models.Context;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Users;
using Neostore.Backend.Infrastructure.Singletons;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Neostore.Backend.Infrastructure.Extensions
{
    public static class UserIdentityExtensions
    {
        public static string GetClientId(this ClaimsPrincipal user)
        {
            return user?.Claims?.FirstOrDefault(c => c.Type == "client_id")?.Value;
        }

        public static string GetUserId(this ClaimsPrincipal user)
        {
            return user?.Claims?.FirstOrDefault(c => c.Type == "sub")?.Value;
        }

        public async static Task<string> GetFirstName(this ClaimsPrincipal user)
        {
            var userBase = await GetUser(user.GetUserId());

            return userBase?.FirstName;
        }

        public async static Task<string> GetLastName(this ClaimsPrincipal user)
        {
            var userBase = await GetUser(user.GetUserId());

            return userBase?.LastName;
        }

        public async static Task<string> GetPhotoUrl(this ClaimsPrincipal user)
        {
            var userBase = await GetUser(user.GetUserId());

            return userBase?.PhotoUrl;
        }

        private async static Task<UserBaseViewModel> GetUser(string userId)
        {
            var userRepository = ServiceProviderSingleton.Instance
                .Provider
                .GetService(typeof(IUserService)) as IUserService;

            var userBase = await userRepository.GetAsync(userId);

            return userBase;
        }
    }
}