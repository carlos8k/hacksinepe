﻿using System;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Neostore.Backend.Infrastructure.Extensions
{
    public static class HtmlExtensions
    {
        public static bool IsCurrentControllerAndAction(this IHtmlHelper html, string actionName, string controllerName)
        {
            bool result = false;
            
            var viewContext = html.ViewContext;

            if (viewContext == null) return false;
            if (string.IsNullOrEmpty(actionName)) return false;
            if (string.IsNullOrEmpty(controllerName)) return false;

            var action = viewContext.RouteData.Values["Action"] as string;
            var controller = viewContext.RouteData.Values["Controller"] as string;

            if( controller.Equals(controllerName, StringComparison.OrdinalIgnoreCase) &&
                action.Equals(actionName, StringComparison.OrdinalIgnoreCase))
            {
                result = true;
            }

            return result;
        }

        public static bool IsCurrentController(this IHtmlHelper html, string controllerName)
        {
            bool result = false;

            var viewContext = html.ViewContext;

            if (viewContext == null) return false;
            if (string.IsNullOrEmpty(controllerName)) return false;

            var action = viewContext.RouteData.Values["Action"] as string;
            var controller = viewContext.RouteData.Values["Controller"] as string;

            if (controller.Equals(controllerName, StringComparison.OrdinalIgnoreCase))
            {
                result = true;
            }

            return result;
        }
    }
}