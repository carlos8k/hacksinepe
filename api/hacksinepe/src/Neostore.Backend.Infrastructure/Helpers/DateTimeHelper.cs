﻿using System;

namespace Neostore.Backend.Infrastructure.Helpers
{
    public static class DateTimeHelper
    {
        public static DateTime ToLocal(this DateTime date)
        {
            var utcNow = DateTime.SpecifyKind(
                        DateTime.UtcNow,
                        DateTimeKind.Utc);

            var dateUtc = DateTime.SpecifyKind(
                date,
                DateTimeKind.Utc);

            var timeZoneString = "E. South America Standard Time";

            var timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneString);

            return TimeZoneInfo.ConvertTime(dateUtc, timeZone);
        }
    }
}