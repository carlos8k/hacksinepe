﻿using System;

namespace Neostore.Backend.Infrastructure.Helpers
{
    public static class GeolocationHelper
    {
        const double RADIO = 6378137;

        public static double GetDistance(double latA, double lngA, double latB, double lngB)
        {
            var dlon = Radians(lngB - lngA);
            var dlat = Radians(latB - latA);

            var a = (Math.Sin(dlat / 2) * Math.Sin(dlat / 2)) + Math.Cos(Radians(latA)) * Math.Cos(Radians(lngA)) * (Math.Sin(dlon / 2) * Math.Sin(dlon / 2));
            var angle = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return angle * RADIO;
        }

        //public static dynamic GetGeometry(string address)
        //{
        //    try
        //    {
        //        var client = new RestClient("https://maps.googleapis.com/maps/");

        //        var request = new RestRequest("api/geocode/json");

        //        request.AddParameter("address", address);
        //        request.AddParameter("key", ConfigurationManager.AppSettings["GoogleMapsApiKey"]);

        //        var response = client.Execute(request);

        //        var location = JsonConvert.DeserializeObject(response.Content) as dynamic;
        //        var geometry = location.results[0].geometry;

        //        return geometry;
        //    }
        //    catch
        //    {
        //        return null;
        //    }
        //}

        private static double Radians(double x)
        {
            return x * Math.PI / 180;
        }
    }
}