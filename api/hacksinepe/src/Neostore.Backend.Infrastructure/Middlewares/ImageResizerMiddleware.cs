﻿using ImageSharp;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.PlatformAbstractions;
using Microsoft.WindowsAzure.Storage;
using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Primitives;

namespace Neostore.Backend.Infrastructure.Middlewares
{
    public class ImageResizerMiddleware
    {
        private readonly object _lock = new object();
        private readonly RequestDelegate _next;

        private HttpContext _httpContext;
        private PreProcessingResults _result;
        private ImageResizerConfiguration _config;
        private IConfigurationRoot _configuration;
        
        public ImageResizerMiddleware(RequestDelegate next, IConfigurationRoot configuration)
        {
            _next = next;
            _configuration = configuration;

            ReadConfig();
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                _httpContext = httpContext;

                PreProcessRequest();

                if (_result.ShouldHandleRequest)
                {
                    if (!_result.OriginalExistsPhysically)
                        await DownloadImage();

                    if (!_result.ResizedExistsPhysically)
                        await ProcessImage();

                    await SendImage();
                }
            }
            finally
            {
                await _next(_httpContext);
            }
        }

        private void ExtractContainerAndBlobNames(out string container, out string blob)
        {
            container = blob = null;

            var localPath = _httpContext.Request.Path.ToString().Replace(@"\", @"/");

            var parts = localPath.Split(new[] {
                Path.DirectorySeparatorChar,
                Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries).ToList();

            if (parts.Count() > 1)
            {
                // A primeira parte é a chave que ativa a execução deste modo (no padrão, "cloudimage").
                // A segunda parte é o container.
                parts.RemoveAt(0);
                container = parts[0];

                // O restante é o blob.
                parts.RemoveAt(0);
                if (parts.Count() > 0)
                    blob = string.Join("/", parts);
            }
        }

        private void CreateFolderStructure()
        {
            var _originalDir = Path.GetDirectoryName(_result.OriginalPhysicalPath);

            if (!Directory.Exists(_originalDir))
            {
                lock (_lock)
                {
                    if (!Directory.Exists(_originalDir))
                    {
                        Directory.CreateDirectory(_originalDir);
                    }
                }
            }

            var _resizeDir = Path.GetDirectoryName(_result.ResizedPhysicalPath);

            if (!Directory.Exists(_resizeDir))
            {
                lock (_lock)
                {
                    if (!Directory.Exists(_resizeDir))
                    {
                        Directory.CreateDirectory(_resizeDir);
                    }
                }
            }
        }

        private async Task DownloadImage()
        {
            string containerName, blobName;
            ExtractContainerAndBlobNames(out containerName, out blobName);

            var storageAccount = CloudStorageAccount.Parse(_config.ConnectionString);
            var blobClient = storageAccount.CreateCloudBlobClient();
            var container = blobClient.GetContainerReference(containerName);
            var blockBlob = container.GetBlockBlobReference(blobName);
            
            using (var fileStream = File.OpenWrite(_result.OriginalPhysicalPath))
            {
                await blockBlob.DownloadToStreamAsync(fileStream);
                _result.OriginalExistsPhysically = File.Exists(_result.OriginalPhysicalPath);
            }
        }

        private void PreProcessRequest()
        {
            _result = new PreProcessingResults();

            if (ShouldProcessRequest())
            {
                string containerName, blobName;
                int width, height;
                
                if (!int.TryParse(_httpContext.Request.Query["w"], out width))
                    int.TryParse(_httpContext.Request.Query["width"], out width);

                if (!int.TryParse(_httpContext.Request.Query["h"], out height))
                    int.TryParse(_httpContext.Request.Query["height"], out height);
                
                _result.Width = width;
                _result.Height = height;
                _result.Mode = _httpContext.Request.Query["mode"];

                if (string.IsNullOrWhiteSpace(_result.Mode))
                    _result.Mode = "pad"; // pad is default mode

                _result.ShouldHandleRequest = true;

                _result.BasePath = PlatformServices.Default.Application.ApplicationBasePath;
                _result.VirtualPath = _httpContext.Request.Path;

                _result.OriginalPhysicalPath = $@"{_result.BasePath}{_result.VirtualPath.Replace(@"/", @"\")}";
                _result.OriginalExistsPhysically = File.Exists(_result.OriginalPhysicalPath);
                
                ExtractContainerAndBlobNames(out containerName, out blobName);

                _result.ContainerName = containerName;
                _result.BlobName = blobName;

                _result.ResizedPhysicalPath = $@"{_result.BasePath}\{_config.Prefix}\{_result.ContainerName}\{_config.ResizedFolder}\{_result.Width}_{_result.Height}_{_result.Mode}_{_result.BlobName}";
                _result.ResizedExistsPhysically = File.Exists(_result.ResizedPhysicalPath);

                CreateFolderStructure();
            }
        }
        
        private Task ProcessImage()
        {
            return Task.Run(() =>
            {
                using (FileStream stream = File.OpenRead(_result.OriginalPhysicalPath))
                using (FileStream output = File.OpenWrite(_result.ResizedPhysicalPath))
                {
                    switch(_result.Mode)
                    {
                        case "stretch":
                            new Image(stream)
                                .Resize(_result.Width, _result.Height)
                                .Save(output);
                            break;
                        case "pad":
                        default:
                            new Image(stream)
                                .Pad(_result.Width, _result.Height)
                                .BackgroundColor(Color.White)
                                .Save(output);
                            break;
                    }
                }
            });
        }

        private void ReadConfig()
        {
            _config = new ImageResizerConfiguration();

            if (_configuration["ImageResizer:Prefix"] != null)
                _config.Prefix = _configuration["ImageResizer:Prefix"];

            if (_configuration["ImageResizer:ResizedFolder"] != null)
                _config.Prefix = _configuration["ImageResizer:ResizedFolder"];

            if (_configuration["ImageResizer:ConnectionString"] != null)
                _config.ConnectionString = _configuration["ImageResizer:ConnectionString"];

            if (_configuration["ImageResizer:ConnectionStringName"] != null)
            {
                var name = _configuration["ImageResizer:ConnectionStringName"];
                _config.ConnectionString = _configuration[name];
            }
        }

        private async Task SendImage()
        {
            var bytes = File.ReadAllBytes(_result.ResizedPhysicalPath);
            await _httpContext.Response.Body.WriteAsync(bytes, 0, bytes.Length);
        }

        private bool ShouldProcessRequest()
        {
            if (_httpContext.Request.Method != "GET")
                return false;

            var parts = _httpContext.Request.Path.ToString().Split(new[] {
                Path.DirectorySeparatorChar,
                Path.AltDirectorySeparatorChar }, StringSplitOptions.RemoveEmptyEntries);

            if (parts.Length < 2)
                return false;

            var prefix = parts.FirstOrDefault();

            if (!prefix?.Equals(_config.Prefix, StringComparison.OrdinalIgnoreCase) ?? false)
                return false;

            var _extension = Path.GetExtension(_httpContext.Request.Path).ToLower();
            if (_extension.Length == 0 || !".bmp.gif.jpg.jpeg.png".Contains(_extension))
                return false;

            return true;
        }

        // Nested types.
        private class PreProcessingResults
        {
            public int Width { get; set; }
            public int Height { get; set; }

            public bool ShouldHandleRequest { get; set; }
            public string VirtualPath { get; set; }

            public string OriginalPhysicalPath { get; set; }
            public string ResizedPhysicalPath { get; set; }

            public bool OriginalExistsPhysically { get; set; }
            public bool ResizedExistsPhysically { get; internal set; }

            public string BasePath { get; set; }

            public string ContainerName { get; set; }
            public string BlobName { get; set; }
            public string Mode { get; set; }
        }

        private class ImageResizerConfiguration
        {
            public string Prefix { get; set; }
            public string ResizedFolder { get; set; }
            public string ConnectionString { get; set; }
            public ImageResizerConfiguration()
            {
                Prefix = "neoimage";
                ResizedFolder = "resized";
            }
        }
    }

    public static class ImageResizerMiddlewareExtensions
    {
        public static IApplicationBuilder UseImageResizer(this IApplicationBuilder builder, IConfigurationRoot configuration)
        {
            return builder.UseMiddleware<ImageResizerMiddleware>(configuration);
        }
    }
}