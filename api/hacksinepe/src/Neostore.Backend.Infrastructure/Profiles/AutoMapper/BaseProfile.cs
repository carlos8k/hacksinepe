﻿using AutoMapper;

namespace Neostore.Backend.Infrastructure.Profiles.AutoMapper
{
    public abstract class BaseProfile : Profile
    {
        private readonly string _profileName;

        protected BaseProfile(string profileName)
        {
            _profileName = profileName;
        }

        public override string ProfileName
        {
            get { return _profileName; }
        }
        
        protected abstract void CreateMaps();
    }
}