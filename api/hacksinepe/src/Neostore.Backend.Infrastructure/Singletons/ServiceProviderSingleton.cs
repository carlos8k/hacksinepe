﻿using System;

namespace Neostore.Backend.Infrastructure.Singletons
{
    public class ServiceProviderSingleton
    {
        private static ServiceProviderSingleton instance = new ServiceProviderSingleton();
        public IServiceProvider Provider { get; set; }
        private ServiceProviderSingleton() { }

        public static ServiceProviderSingleton Instance
        {
            get
            {
                return instance;
            }
        }

        public void Configure(IServiceProvider provider)
        {
            Provider = provider;
        }
    }
}