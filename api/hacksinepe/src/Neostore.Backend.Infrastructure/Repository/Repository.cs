﻿using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Interfaces.Repository;
using Neostore.Backend.Domain.Models.Context;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.Infrastructure.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected BackendContext Context { get; set; }

        public Repository(BackendContext context)
        {
            Context = context;
        }

        private DbSet<T> _entities;

        private DbSet<T> Entities
        {
            get
            {
                if (_entities == null)
                {
                    _entities = Context.Set<T>();
                }
                return _entities;
            }
        }

        public T GetById(object id)
        {
            throw new NotImplementedException();
            //var entity = Entities.Find(id);

            //return entity;
        }

        public void Insert(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            Entities.Add(entity);
        }

        public void Update(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            Context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(T entity)
        {
            if (entity == null)
                throw new ArgumentNullException("entity");

            Context.Entry(entity).State = EntityState.Deleted;
        }

        public IQueryable<T> List
        {
            get { return Entities; }
        }

        public void Attach(object entity)
        {
            if (entity != null)
                Context.Entry(entity).State = EntityState.Unchanged;
        }

        public void SetObjectState(object entity, EntityState state)
        {
            if (entity != null)
                Context.Entry(entity).State = state;
        }

        public async Task SaveChangesAsync()
        {
            await Context.SaveChangesAsync();
        }
    }
}