﻿using Microsoft.Extensions.DependencyInjection;
using Neostore.Backend.Business.Account;
using Neostore.Backend.Business.Custom;
using Neostore.Backend.Business.User;
using Neostore.Backend.Domain.Interfaces.Custom;
using Neostore.Backend.Domain.Interfaces.Services.Account;
using Neostore.Backend.Domain.Interfaces.Services.User;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Users;

namespace Neostore.Backend.API
{
    public partial class Startup
    {
        private void ConfigureBusinessServices(IServiceCollection services)
        {
            // account
            services.AddTransient<ILoginService, LoginService>();

            // user
            services.AddTransient<IUserService, UserService>();
            services.AddTransient<IRegisterService<Seller, SellerViewModel>, SellerRegisterService>();

            // custom
            services.AddTransient<IEvaluationService, EvaluationService>();
            services.AddTransient<IReasonService, ReasonService>();
        }
    }
}