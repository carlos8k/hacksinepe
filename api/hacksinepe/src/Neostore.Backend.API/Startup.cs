﻿using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Neostore.Backend.Domain.Interfaces.Repository;
using Neostore.Backend.Domain.Models.Context;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Infrastructure.Config;
using Neostore.Backend.Infrastructure.Repository;
using Neostore.Backend.Infrastructure.Singletons;
using Swashbuckle.Swagger.Model;
using System.Reflection;

namespace Neostore.Backend.API
{
    /// <summary>
    /// Application startup class
    /// </summary>
    public partial class Startup
    {
        /// <summary>
        /// Configuration injection instance
        /// </summary>
        public IConfigurationRoot Configuration { get; }

        /// <summary>
        /// Application startup method
        /// </summary>
        /// <param name="env"></param>
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            
            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("BackendContext");
            var migrationsAssembly = typeof(Startup).GetTypeInfo().Assembly.GetName().Name;

            // register application context
            services.AddDbContext<BackendContext>(options =>
                options.UseSqlServer(connectionString));
            
            // register identity
            services.AddIdentity<UserBase, IdentityRole>(options =>
                {
                    options.Password.RequireDigit = false;
                    options.Password.RequireLowercase = false;
                    options.Password.RequireUppercase = false;
                    options.Password.RequireNonAlphanumeric = false;
                    options.Password.RequiredLength = 4;
                    options.Cookies.ApplicationCookie.AccessDeniedPath = options.Cookies.ApplicationCookie.LoginPath;
                })
               .AddEntityFrameworkStores<BackendContext>()
               .AddDefaultTokenProviders();

            // register generic repository
            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            ConfigureBusinessServices(services);

            // register automapper
            services.AddSingleton(AutoMapperConfig.RegisterMaps());

            services.AddApplicationInsightsTelemetry(Configuration);
            services.AddMvc();
            services.AddSwaggerGen();

            services.Configure<MvcOptions>(options =>
            {
                options.Filters.Add(new RequireHttpsAttribute());
            });

            services.AddIdentityServer()
                .AddTemporarySigningCredential()
                .AddConfigurationStore(builder =>
                    builder.UseSqlServer(connectionString, options =>
                        options.MigrationsAssembly(migrationsAssembly)))
                .AddOperationalStore(builder =>
                    builder.UseSqlServer(connectionString, options =>
                        options.MigrationsAssembly(migrationsAssembly)))
                .AddAspNetIdentity<UserBase>();

            services.ConfigureSwaggerGen(options =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = Configuration["Aplication:Name"],
                    Description = Configuration["Aplication:Description"],
                    Contact = new Contact { Name = Configuration["Aplication:Contact"], Email = Configuration["Aplication:Email"] },
                    License = new License { Name = Configuration["Aplication:License"], Url = Configuration["Aplication:Site"] }
                });

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;

                options.IncludeXmlComments(basePath + "\\Neostore.Backend.API.xml");
                options.DescribeAllEnumsAsStrings();
            });

            services.Configure<RouteOptions>(options => options.LowercaseUrls = true);
            
            // allow access service provider in infrastructure project
            ServiceProviderSingleton.Instance.Configure(services.BuildServiceProvider());
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        /// <param name="loggerFactory"></param>
        /// <param name="contextApplication"></param>
        /// <param name="contextIdentityServerConfiguration"></param>
        /// <param name="contextIdentityServerGrants"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            BackendContext contextApplication, ConfigurationDbContext contextIdentityServerConfiguration, PersistedGrantDbContext contextIdentityServerGrants)
        {
            app.UseStaticFiles();

            ConfigureLogger(loggerFactory);
            ConfigureDevelopment(app, env);
            ConfigureDatabases(contextApplication, contextIdentityServerConfiguration, contextIdentityServerGrants);
            ConfigureAuth(app);
            
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}");

                routes.MapRoute(
                    name: "admin",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
            
            app.UseSwagger();
            app.UseSwaggerUi();
        }
    }
}