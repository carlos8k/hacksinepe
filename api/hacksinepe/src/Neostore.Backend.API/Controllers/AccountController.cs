﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Neostore.Backend.Domain.Interfaces.Services.Account;
using Neostore.Backend.Domain.ViewModels.Pages;
using Neostore.Backend.API.Controllers;

namespace API.Controllers
{
    /// <summary>
    /// Manage auth and register users
    /// </summary>
    public class AccountController : Controller
    {
        ILoginService loginService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="_loginService">Login business service</param>
        public AccountController(ILoginService _loginService)
        {
            loginService = _loginService;
        }

        /// <summary>
        /// Manage login users
        /// </summary>
        /// <returns></returns>
        public IActionResult Login(string returnUrl = null)
        {
            ViewData["ReturnUrl"] = returnUrl;
            return View();
        }

        /// <summary>
        /// Do the login
        /// </summary>
        /// <param name="model"></param>
        /// /// <param name="returnUrl"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model, string returnUrl = null)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    await loginService.SignInAsync(model.UserName, model.Password);
                    return RedirectToLocal(returnUrl);
                }
                catch (InvalidOperationException ex)
                {
                    ModelState.AddModelError("Username", ex.Message);
                }
            }

            return View(model);
        }

        /// <summary>
        /// Do the logoff
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Logoff()
        {
            await loginService.SignOutAsync();
            return RedirectToLocal(null);
        }

        private IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction(nameof(RedirectController.Index), "Redirect");
            }
        }
    }
}