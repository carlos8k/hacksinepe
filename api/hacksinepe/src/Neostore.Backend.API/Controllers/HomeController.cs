﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;

namespace API.Controllers
{
    /// <summary>
    /// Application web entry point
    /// </summary>
    public class HomeController : Controller
    {
        /// <summary>
        /// Initial page
        /// </summary>
        /// <returns></returns>
        [Authorize]
        public IActionResult Index()
        {
            return RedirectToAction("Index", "Redirect");
        }
    }
}