﻿using Microsoft.AspNetCore.Mvc;

namespace Neostore.Backend.API.Controllers
{
    /// <summary>
    /// Manage auth redirects
    /// </summary>
    public class RedirectController : Controller
    {
        /// <summary>
        /// Send a user to property page
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            if(User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Likes", new { Area = "Admin" });

            return RedirectToAction("Login", "Account", new { Area = "" });
        }
    }
}