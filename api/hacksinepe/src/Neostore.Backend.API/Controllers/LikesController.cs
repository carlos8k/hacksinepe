﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Neostore.Backend.Domain.Models;
using Neostore.Backend.Domain.Models.Context;
using Neostore.Backend.Infrastructure.Extensions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.API.Controllers
{
    /// <summary>
    /// Manage likes
    /// </summary>
    [Route("api/[controller]")]
    [Authorize]
    public class LikesController : Controller
    {
        BackendContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        public LikesController(BackendContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Create likes
        /// </summary>
        /// <returns></returns>
        [HttpPost("{contentId}")]
        public async Task<IActionResult> Post(int contentId)
        {
            try
            {
                _context.Likes.Add(new Like
                {
                    Content = _context.Contents.FirstOrDefault(c => c.Id == contentId),
                    Owner = _context.Users.FirstOrDefault(u => u.Id == User.GetUserId()),
                    IsLike = true
                });

                await _context.SaveChangesAsync();

                return Created("", null);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
