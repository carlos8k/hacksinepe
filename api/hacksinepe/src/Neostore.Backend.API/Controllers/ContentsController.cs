﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Models;
using Neostore.Backend.Domain.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.API.Controllers
{
    /// <summary>
    /// manage contents
    /// </summary>
    [Route("api/[controller]")]
    public class ContentsController : Controller
    {
        BackendContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        public ContentsController(BackendContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all contents
        /// </summary>
        [HttpGet("{categoryId}")]
        [Produces(typeof(IEnumerable<Content>))]
        public IActionResult Get(int categoryId)
        {
            try
            {
                var contents = _context.Contents
                    .Where(c => c.CategoryId == categoryId)
                    .ToList();

                return Ok(contents);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }
    }
}
