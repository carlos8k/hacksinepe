﻿using Microsoft.Extensions.PlatformAbstractions;
using Swashbuckle.Swagger.Model;
using Swashbuckle.SwaggerGen.Application;
using System;

namespace Neostore.Backend.API
{
    public partial class Startup
    {
        private Action ConfigureSwaggerGenOptions(SwaggerGenOptions options)
        {
            return () =>
            {
                options.SingleApiVersion(new Info
                {
                    Version = "v1",
                    Title = Configuration["Neostore:Name"],
                    Description = Configuration["Neostore:Description"],
                    Contact = new Contact { Name = Configuration["Neostore:Contact"], Email = Configuration["Neostore:Email"] },
                    License = new License { Name = Configuration["Neostore:License"], Url = Configuration["Neostore:Site"] }
                });

                var basePath = PlatformServices.Default.Application.ApplicationBasePath;

                options.IncludeXmlComments(basePath + "\\Neostore.Backend.API.xml");

                throw new Exception(basePath);
            };
        }
    }
}