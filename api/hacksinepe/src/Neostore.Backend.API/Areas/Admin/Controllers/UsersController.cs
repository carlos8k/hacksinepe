﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Neostore.Backend.Domain.Interfaces.Services.User;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Users;
using System.Threading.Tasks;

namespace Neostore.Backend.API.Areas.Admin.Controllers
{
    /// <summary>
    /// Manage users
    /// </summary>
    [Area("Admin")]
    [Authorize]
    [Authorize(Roles = "Admin")]
    public class UsersController : Controller
    {
        IMapper _mapper;
        IRegisterService<Seller, SellerViewModel> _sellerService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="sellerService"></param>
        public UsersController(IMapper mapper,
            IRegisterService<Seller, SellerViewModel> sellerService)
        {
            _mapper = mapper;
            _sellerService = sellerService;
        }

        /// <summary>
        /// Get list of reasons
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var reasons = await _sellerService.GetAsync();

            return View(reasons);
        }

        /// <summary>
        /// Create new reason view page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Create a new reason
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SellerViewModel model)
        {
            if (ModelState.IsValid)
            {
                var reason = await _sellerService.CreateAsync(model);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        /// <summary>
        /// Get reason to update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Edit(string id)
        {
            var reason = await _sellerService.GetAsync(id);

            return View(reason);
        }

        /// <summary>
        /// Update a reason
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, SellerViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _sellerService.UpdateAsync(id, model);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        /// <summary>
        /// Delete a reason
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(string id)
        {
            await _sellerService.DeleteAsync(id);
            return RedirectToAction("Index");
        }
    }
}