﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Models.Context;
using Neostore.Backend.Domain.Models.Users;
using Neostore.Backend.Domain.ViewModels.Custom;
using Neostore.Backend.Infrastructure.Extensions;
using System.Linq;

namespace Neostore.Backend.API.Areas.Admin.Controllers
{
    /// <summary>
    /// Evaluations manager
    /// </summary>
    [Area("Admin")]
    [Authorize(Roles = "Admin, Seller")]
    public class EvaluationsController : Controller
    {
        BackendContext _context;
        IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mapper"></param>
        public EvaluationsController(BackendContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Get evaluation list
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var returnAll = _context.Users
                .OfType<AdminUser>()
                .Any(u => u.Id == User.GetUserId());
            
            var evaluations = _context.Evaluations
                .Where(e => e.Seller.Id == User.GetUserId() || returnAll)
                .Include(e => e.Buyer)
                .Include(e => e.Seller)
                .Include(e => e.EvaluationReasons)
                    .ThenInclude(e => e.Reason)
                .ToList();

            var model = evaluations
                .Select(e => _mapper.Map<EvaluationViewModel>(e))
                .ToList();

            foreach (var item in model)
            {
                foreach (var evaluationReason in item.EvaluationReasons)
                {
                    item.Reasons.Add(_mapper.Map<ReasonViewModel>(evaluationReason.Reason));
                }
            }

            model = model.OrderByDescending(m => m.CreateDate).ToList();

            return View(model);
        }
    }
}