﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Models;
using Neostore.Backend.Domain.Models.Context;
using Neostore.Backend.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.API.Areas.Admin.Controllers
{
    /// <summary>
    /// Manage likes
    /// </summary>
    [Area("Admin")]
    [Authorize(Roles = "Admin, Seller")]
    public class LikesController : Controller
    {
        BackendContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        /// <param name="mapper"></param>
        public LikesController(BackendContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get evaluation list
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            var likesCount = _context.Likes
                .GroupBy(l => l.ContentId)
                .Select(group => new
                {
                    ContentId = group.Key,
                    Count = group.Count()
                })
                .OrderBy(x => x.ContentId)
                .ToList();

            var model = new List<Content>();

            foreach (var item in likesCount)
            {
                var content = _context.Contents.FirstOrDefault(c => c.Id == item.ContentId);

                content.LikeCount = item.Count;

                model.Add(content);
            }

            return View(model);
        }
    }
}