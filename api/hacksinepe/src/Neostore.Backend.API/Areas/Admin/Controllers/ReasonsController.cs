﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Neostore.Backend.Domain.Interfaces.Custom;
using Neostore.Backend.Domain.ViewModels.Custom;
using System.Threading.Tasks;

namespace Neostore.Backend.API.Areas.Admin.Controllers
{
    /// <summary>
    /// Manage reasons
    /// </summary>
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ReasonsController : Controller
    {
        IMapper _mapper;
        IReasonService _reasonService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="reasonService"></param>
        public ReasonsController(IMapper mapper,
            IReasonService reasonService)
        {
            _mapper = mapper;
            _reasonService = reasonService;
        }

        /// <summary>
        /// Get list of reasons
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var reasons = await _reasonService.GetAsync();
            
            return View(reasons);
        }

        /// <summary>
        /// Create new reason view page
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// Create a new reason
        /// </summary>
        /// <param name="model"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ReasonViewModel model)
        {
            if(ModelState.IsValid)
            {
                var reason = await _reasonService.CreateAsync(model);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        /// <summary>
        /// Get reason to update
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            var reason = await _reasonService.GetAsync(id);

            return View(reason);
        }

        /// <summary>
        /// Update a reason
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, ReasonViewModel model)
        {
            if (ModelState.IsValid)
            {
                await _reasonService.UpdateAsync(id, model);

                return RedirectToAction("Index");
            }

            return View(model);
        }

        /// <summary>
        /// Delete a reason
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<IActionResult> Delete(int id)
        {
            await _reasonService.DeleteAsync(id);
            return RedirectToAction("Index");
        }
    }
}