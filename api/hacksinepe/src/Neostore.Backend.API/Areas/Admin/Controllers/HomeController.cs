﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Neostore.Backend.Domain.ViewModels.Pages;
using Neostore.Backend.Business.Reports;
using Neostore.Backend.Domain.Models.Context;
using System.Linq;
using System.Threading.Tasks;

namespace Neostore.Backend.API.Areas.Admin.Controllers
{
    /// <summary>
    /// Admin home controller
    /// </summary>
    [Area("Admin")]
    [Authorize(Roles = "Admin, Seller")]
    public class HomeController : Controller
    {
        BackendContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public HomeController(BackendContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Index page
        /// </summary>
        /// <returns></returns>
        public async Task<IActionResult> Index()
        {
            var model = new DashboardViewModel();
            var service = new EvaluationReportService(_context);

            model.EvaluationPercents = (await service.GetEvaluationPercents()).ToList();
            model.EvaluationsAverage = await service.GetEvaluationsAverage();
            model.EvaluationsCount = await service.GetEvaluationsCount();
            model.EvaluationsCountAverage = await service.GetEvaluationsCountAverage();
            model.EvaluationsCountMax = await service.GetEvaluationsCountMax();
            model.EvaluationsCountMin = await service.GetEvaluationsCountMin();

            return View(model);
        }
    }
}