﻿using Microsoft.Extensions.Logging;

namespace Neostore.Backend.API
{
    public partial class Startup
    {
        private void ConfigureLogger(ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();
        }
    }
}