﻿using Neostore.Backend.Domain.Models.Context;
using IdentityServer4.EntityFramework.DbContexts;
using Microsoft.EntityFrameworkCore;

namespace Neostore.Backend.API
{
    public partial class Startup
    {
        private void ConfigureDatabases(BackendContext contextApplication, ConfigurationDbContext contextIdentityServerConfiguration, PersistedGrantDbContext contextIdentityServerGrants)
        {
            BackendContextInitializer.Initialize(contextApplication, Configuration);
            contextIdentityServerGrants.Database.Migrate();
            IdentityServerContextInitializer.Initialize(contextIdentityServerConfiguration, Configuration);
        }
    }
}