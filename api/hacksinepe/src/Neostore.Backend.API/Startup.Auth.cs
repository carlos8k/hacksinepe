﻿using Microsoft.AspNetCore.Builder;
using System.IdentityModel.Tokens.Jwt;

namespace Neostore.Backend.API
{
    public partial class Startup
    {
		private void ConfigureAuth(IApplicationBuilder app)
        {
            // api
            app.MapWhen(context2 => context2.Request.Path.Value.StartsWith("/api"), builder =>
            {
                builder.UseIdentityServerAuthentication(new IdentityServerAuthenticationOptions
                {
                    Authority = Configuration["Application:Url"],
                    AllowedScopes = new[] { Configuration["Application:Scope"] },
                    RequireHttpsMetadata = Configuration["Application:Url"].ToLower().StartsWith("https"),
                    SaveToken = true,
                    EnableCaching = true
                });

                builder.UseMvc();

            });

            // sts
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            app.UseIdentity();
            app.UseIdentityServer();
        }
    }
}