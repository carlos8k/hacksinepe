﻿using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;

namespace Neostore.Backend.Domain
{
    /// <summary>
    /// Program start point
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Program main method
        /// </summary>
        /// <param name="args"></param>
        public static void Main(string[] args)
        {
            
        }
    }
}