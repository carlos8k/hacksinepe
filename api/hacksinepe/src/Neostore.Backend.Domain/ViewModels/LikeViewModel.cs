﻿using Neostore.Backend.Domain.Models;
using Neostore.Backend.Domain.Models.Users;

namespace Neostore.Backend.Domain.ViewModels
{
    public class LikeViewModel
    {
        public int Id { get; set; }
        public UserBase Owner { get; set; }
        public Content Content { get; set; }
        public int Count { get; set; }
        public int ContentId { get; set; }
    }
}