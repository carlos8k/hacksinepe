﻿using System.Collections.Generic;

namespace Neostore.Backend.Domain.ViewModels.Pages
{
    public class DashboardViewModel
    {
        /// <summary>
        /// Média de avaliação geral (média entre 5,4,3,2,1)
        /// </summary>
        public decimal EvaluationsAverage { get; set; }

        /// <summary>
        /// Percentual de votos em 5, 4, 3, 2, 1
        /// </summary>
        public List<double> EvaluationPercents { get; set; }

        /// <summary>
        /// Total de avaliações únicas
        /// </summary>
        public int EvaluationsCount { get; set; }

        /// <summary>
        /// Média de avaliações por loja
        /// </summary>
        public int EvaluationsCountAverage { get; set; }

        /// <summary>
        /// Quantidade de avaliações da loja mais avaliada
        /// </summary>
        public int EvaluationsCountMax { get; set; }

        /// <summary>
        /// Quantidade de avaliações a loja menos avalida
        /// </summary>
        public int EvaluationsCountMin { get; set; }
    }
}