﻿using System.ComponentModel.DataAnnotations;

namespace Neostore.Backend.Domain.ViewModels.Custom
{
    public class ReasonViewModel
    {
        public int Id { get; set; }
        [Display(Name = "O que poderia ser melhor?")]
        public string Description { get; set; }
    }
}