﻿namespace Neostore.Backend.Domain.ViewModels.Custom
{
    public class EvaluationReasonViewModel
    {
        public int ReasonId { get; set; }
        public int EvaluationId { get; set; }

        public virtual ReasonViewModel Reason { get; set; }
        public virtual EvaluationViewModel Evaluation { get; set; }
    }
}