﻿using Neostore.Backend.Domain.ViewModels.Users;
using System;
using System.Collections.Generic;

namespace Neostore.Backend.Domain.ViewModels.Custom
{
    public class EvaluationViewModel
    {
        public int Rating { get; set; }
        public DateTime CreateDate { get; set; }

        public BuyerViewModel Buyer { get; set; }
        public SellerViewModel Seller { get; set; }
        public List<ReasonViewModel> Reasons { get; set; } = new List<ReasonViewModel>();
        public List<EvaluationReasonViewModel> EvaluationReasons { get; set; } = new List<EvaluationReasonViewModel>();
    }
}