﻿using Neostore.Backend.Domain.ViewModels.Location;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Neostore.Backend.Domain.ViewModels.Users
{
    public class UserBaseViewModel
    {
        public string Id { get; set; }

        public string ClientId { get; set; }

        [Required]
        public string Email { get; set; }

        [Display(Name = "Senha")]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Nome")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Sobrenome")]
        public string LastName { get; set; }

        public string PhotoUrl { get; set; }

        public List<AddressViewModel> Addresses { get; set; }

        public double Distance(double latitude, double longitude)
        {
            return new Random().Next(10, 100);
        }
    }
}