﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Neostore.Backend.Domain.Models.Context;

namespace Neostore.Backend.Domain.Data.Migrations.Application.BackendContextDb
{
    [DbContext(typeof(BackendContext))]
    [Migration("20170820071213_Likes")]
    partial class Likes
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "1.1.2")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasName("RoleNameIndex");

                    b.ToTable("AspNetRoles");

                    b.HasAnnotation("SqlServer:TableName", "Roles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("RoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims");

                    b.HasAnnotation("SqlServer:TableName", "UserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins");

                    b.HasAnnotation("SqlServer:TableName", "UserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles");

                    b.HasAnnotation("SqlServer:TableName", "UserRoles");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("LoginProvider");

                    b.Property<string>("Name");

                    b.Property<string>("Value");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("UserTokens");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Content", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("CategoryId");

                    b.Property<string>("ImageUrl");

                    b.Property<string>("Text");

                    b.HasKey("Id");

                    b.ToTable("Contents");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Custom.Evaluation", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BuyerId");

                    b.Property<DateTime>("CreateDate");

                    b.Property<int>("Rating");

                    b.Property<string>("SellerId");

                    b.HasKey("Id");

                    b.HasIndex("BuyerId");

                    b.HasIndex("SellerId");

                    b.ToTable("Evaluations");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Custom.EvaluationReason", b =>
                {
                    b.Property<int>("ReasonId");

                    b.Property<int>("EvaluationId");

                    b.HasKey("ReasonId", "EvaluationId");

                    b.HasIndex("EvaluationId");

                    b.ToTable("EvaluationReason");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Custom.Reason", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Description");

                    b.Property<bool>("IsDeleted");

                    b.HasKey("Id");

                    b.ToTable("Reasons");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Like", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("ContentId");

                    b.Property<bool>("IsLike");

                    b.Property<string>("OwnerId");

                    b.HasKey("Id");

                    b.HasIndex("ContentId");

                    b.HasIndex("OwnerId");

                    b.ToTable("Likes");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Users.IdentityClient", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClientId");

                    b.Property<string>("Name");

                    b.HasKey("Id");

                    b.ToTable("IdentityClient");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Users.UserBase", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<DateTime>("CreateDate");

                    b.Property<string>("DeviceToken");

                    b.Property<string>("Discriminator")
                        .IsRequired();

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<string>("FacebookId");

                    b.Property<string>("FacebookToken");

                    b.Property<string>("FirstName")
                        .IsRequired();

                    b.Property<int?>("IdentityClientId");

                    b.Property<bool>("IsDeleted");

                    b.Property<string>("LastName")
                        .IsRequired();

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .IsRequired()
                        .HasMaxLength(256);

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("ProfilePhotoUrl");

                    b.Property<string>("SecurityStamp");

                    b.Property<string>("TimeZone");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasMaxLength(256);

                    b.HasKey("Id");

                    b.HasIndex("IdentityClientId");

                    b.HasIndex("NormalizedEmail")
                        .HasName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasName("UserNameIndex");

                    b.ToTable("AspNetUsers");

                    b.HasDiscriminator<string>("Discriminator").HasValue("UserBase");

                    b.HasAnnotation("SqlServer:TableName", "Users");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Users.AdminUser", b =>
                {
                    b.HasBaseType("Neostore.Backend.Domain.Models.Users.UserBase");


                    b.ToTable("AdminUser");

                    b.HasDiscriminator().HasValue("AdminUser");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Users.Buyer", b =>
                {
                    b.HasBaseType("Neostore.Backend.Domain.Models.Users.UserBase");

                    b.Property<string>("Cnpj");

                    b.Property<string>("CompanyName");

                    b.Property<string>("Cpf");

                    b.Property<int?>("Gender");

                    b.ToTable("Buyer");

                    b.HasDiscriminator().HasValue("Buyer");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Users.Seller", b =>
                {
                    b.HasBaseType("Neostore.Backend.Domain.Models.Users.UserBase");

                    b.Property<string>("Cnpj");

                    b.Property<string>("CompanyName");

                    b.Property<string>("Cpf");

                    b.Property<int?>("Gender");

                    b.ToTable("Seller");

                    b.HasDiscriminator().HasValue("Seller");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Claims")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Neostore.Backend.Domain.Models.Users.UserBase")
                        .WithMany("Claims")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Neostore.Backend.Domain.Models.Users.UserBase")
                        .WithMany("Logins")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.EntityFrameworkCore.IdentityRole")
                        .WithMany("Users")
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Neostore.Backend.Domain.Models.Users.UserBase")
                        .WithMany("Roles")
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Custom.Evaluation", b =>
                {
                    b.HasOne("Neostore.Backend.Domain.Models.Users.Buyer", "Buyer")
                        .WithMany()
                        .HasForeignKey("BuyerId");

                    b.HasOne("Neostore.Backend.Domain.Models.Users.Seller", "Seller")
                        .WithMany()
                        .HasForeignKey("SellerId");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Custom.EvaluationReason", b =>
                {
                    b.HasOne("Neostore.Backend.Domain.Models.Custom.Evaluation", "Evaluation")
                        .WithMany("EvaluationReasons")
                        .HasForeignKey("EvaluationId")
                        .OnDelete(DeleteBehavior.Cascade);

                    b.HasOne("Neostore.Backend.Domain.Models.Custom.Reason", "Reason")
                        .WithMany("EvaluationReasons")
                        .HasForeignKey("ReasonId")
                        .OnDelete(DeleteBehavior.Cascade);
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Like", b =>
                {
                    b.HasOne("Neostore.Backend.Domain.Models.Content", "Content")
                        .WithMany()
                        .HasForeignKey("ContentId");

                    b.HasOne("Neostore.Backend.Domain.Models.Users.UserBase", "Owner")
                        .WithMany()
                        .HasForeignKey("OwnerId");
                });

            modelBuilder.Entity("Neostore.Backend.Domain.Models.Users.UserBase", b =>
                {
                    b.HasOne("Neostore.Backend.Domain.Models.Users.IdentityClient", "IdentityClient")
                        .WithMany()
                        .HasForeignKey("IdentityClientId");
                });
        }
    }
}
