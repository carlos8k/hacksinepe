﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Neostore.Backend.Domain.Models.Custom;
using Neostore.Backend.Domain.Models.Users;
using System;
using System.Linq;

namespace Neostore.Backend.Domain.Models.Context
{
    public static class BackendContextInitializer
    {
        public static void Initialize(BackendContext context, IConfigurationRoot configuration)
        {
            context.Database.Migrate();

            if (!context.Roles.Any())
            {
                context.Roles.Add(new IdentityRole { Name = "Admin", NormalizedName = "Admin".ToUpper() });
                context.Roles.Add(new IdentityRole { Name = "Buyer", NormalizedName = "Buyer".ToUpper() });
                context.Roles.Add(new IdentityRole { Name = "Seller", NormalizedName = "Seller".ToUpper() });
            }

            if (!context.Users.Any())
            {
                var passwordHash = new PasswordHasher<UserBase>();

                var adminUser = new AdminUser
                {
                    FirstName = "Boticário",
                    LastName = "Encantômetro",
                    Email = "encantometro@boticario.com.br",
                    NormalizedEmail = "encantometro@boticario.com.br".ToUpper(),
                    UserName = "encantometro@boticario.com.br",
                    NormalizedUserName = "encantometro@boticario.com.br".ToUpper(),
                    SecurityStamp = Guid.NewGuid().ToString(),
                    CreateDate = DateTime.UtcNow
                };
                
                adminUser.PasswordHash = passwordHash.HashPassword(adminUser, "boti@2016");

                context.SaveChanges();

                var role = context.Roles.Include(r => r.Users).FirstOrDefault(u => u.Name == "Admin");

                if (!role.Users.Any(u => u.UserId == adminUser.Id))
                    role.Users.Add(new IdentityUserRole<string> { UserId = adminUser.Id, RoleId = role.Id });

                context.Users.Add(adminUser);
            }

            if (!context.Reasons.Any())
            {
                context.Reasons.AddRange(new[]
                {
                    new Reason { Description = "Conhecimento sobre os produtos" },
                    new Reason { Description = "Entendimento da minha necessidade" },
                    new Reason { Description = "Experimentar mais produtos" },
                    new Reason { Description = "Simpatia e atenção" },
                    new Reason { Description = "Outros" },
                    new Reason { Description = "Está tudo ótimo :)" }
                });
            }

            context.SaveChanges();
        }
    }
}