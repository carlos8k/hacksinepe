﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Mappers;
using IdentityServer4.Models;
using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Linq;

namespace Neostore.Backend.Domain.Models.Context
{
    public static class IdentityServerContextInitializer
    {
        public static void Initialize(ConfigurationDbContext context, IConfigurationRoot configuration)
        {
            context.IdentityResources.RemoveRange(context.IdentityResources);
            context.ApiResources.RemoveRange(context.ApiResources);
            context.Clients.RemoveRange(context.Clients);

            context.SaveChanges();

            if (!context.IdentityResources.Any())
            {
                foreach (var resource in GetIdentityScopes())
                    context.IdentityResources.Add(resource.ToEntity());

                context.SaveChanges();
            }

            if (!context.ApiResources.Any())
            {
                context.ApiResources.Add(new ApiResource
                {
                    Name = configuration["Application:Scope"],
                    ApiSecrets = new[] { new Secret { Value = configuration["Application:ScopeSecret"].Sha256() } },
                    Scopes = new[] { new Scope { Name = configuration["Application:Scope"] } },
                    DisplayName = configuration["Application:Name"],
                    Description = configuration["Application:Description"],
                    Enabled = true
                }.ToEntity());

                context.SaveChanges();
            }

            if (!context.Clients.Any())
            {
                context.Clients.Add(new Client
                {
                    ClientId = configuration["Application:ClientId"],
                    ClientName = configuration["Application:Name"],
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPasswordAndClientCredentials,

                    ClientSecrets =
                    {
                        new Secret(configuration["Application:ClientSecret"].Sha256())
                    },
                    AllowedScopes = { configuration["Application:Scope"] }
                }.ToEntity());

                context.SaveChanges();
            }
        }

        public static IEnumerable<IdentityResource> GetIdentityScopes()
        {
            return new IdentityResource[]
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Email(),
                new IdentityResources.Address(),
                new IdentityResource("roles", new[] { "role" })
            };
        }
    }
}