﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Neostore.Backend.Domain.Models.Custom;
using Neostore.Backend.Domain.Models.Users;

namespace Neostore.Backend.Domain.Models.Context
{
    public class BackendContext : IdentityDbContext<UserBase>
    {
        public BackendContext(DbContextOptions<BackendContext> options) : base(options) { }
        
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<UserBase>().ForSqlServerToTable("Users");
            builder.Entity<IdentityUserClaim<string>>().ForSqlServerToTable("UserClaims");
            builder.Entity<IdentityUserLogin<string>>().ForSqlServerToTable("UserLogins");
            builder.Entity<IdentityUserRole<string>>().ForSqlServerToTable("UserRoles");
            builder.Entity<IdentityUserToken<string>>().ToTable("UserTokens");
            builder.Entity<IdentityRoleClaim<string>>().ToTable("RoleClaims");
            builder.Entity<IdentityRole>().ForSqlServerToTable("Roles");

            builder.Entity<EvaluationReason>()
                .HasKey(x => new { x.ReasonId, x.EvaluationId });

            builder.Entity<EvaluationReason>()
                .HasOne(pt => pt.Evaluation)
                .WithMany(p => p.EvaluationReasons)
                .HasForeignKey(pt => pt.EvaluationId);

            builder.Entity<EvaluationReason>()
                .HasOne(pt => pt.Reason)
                .WithMany(t => t.EvaluationReasons)
                .HasForeignKey(pt => pt.ReasonId);
        }

        // users
        public DbSet<IdentityClient> IdentityClient { get; set; }
        public DbSet<AdminUser> AdminUsers { get; set; }

        // custom
        public DbSet<Evaluation> Evaluations { get; set; }
        public DbSet<Reason> Reasons { get; set; }
        public DbSet<Content> Contents { get; set; }
        public DbSet<Like> Likes { get; set; }
    }
}