﻿using IdentityServer4.EntityFramework.DbContexts;
using IdentityServer4.EntityFramework.Options;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;
using System.Reflection;

namespace Neostore.Backend.Domain.Models.Context
{
    public class ConfigurationDbContextFactory : IDbContextFactory<ConfigurationDbContext>
    {
        public ConfigurationDbContext Create(DbContextFactoryOptions options)
        {
            var migrationsAssembly = typeof(PersistedGrantDbContextFactory).GetTypeInfo().Assembly.GetName().Name;

            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(options.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{options.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            
            var config = configurationBuilder.Build();
            var connectionString = config.GetConnectionString("BackendContext");
            var contextBuilder = new DbContextOptionsBuilder<ConfigurationDbContext>();

            contextBuilder.UseSqlServer(connectionString, sqlOptions => sqlOptions.MigrationsAssembly(migrationsAssembly));

            return new ConfigurationDbContext(contextBuilder.Options, new ConfigurationStoreOptions {  });
        }
    }
}