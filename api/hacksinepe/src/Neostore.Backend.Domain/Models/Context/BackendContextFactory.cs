﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.Extensions.Configuration;

namespace Neostore.Backend.Domain.Models.Context
{
    public class BackendContextFactory : IDbContextFactory<BackendContext>
    {
        public BackendContext Create(DbContextFactoryOptions options)
        {
            var configurationBuilder = new ConfigurationBuilder()
                .SetBasePath(options.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{options.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            
            var config = configurationBuilder.Build();
            var connectionString = config.GetConnectionString("BackendContext");
            var contextBuilder = new DbContextOptionsBuilder<BackendContext>();

            contextBuilder.UseSqlServer(connectionString);

            return new BackendContext(contextBuilder.Options);
        }
    }
}