﻿using Neostore.Backend.Domain.Models.Users;

namespace Neostore.Backend.Domain.Models
{
    public class Like
    {
        public int Id { get; set; }
        public bool IsLike { get; set; }
        public UserBase Owner { get; set; }
        public Content Content { get; set; }
        public int ContentId { get; set; }
    }
}