﻿using Neostore.Backend.Domain.Enum;

namespace Neostore.Backend.Domain.Models.Users
{
    public class Person : UserBase
    {
        // individual
        public Gender? Gender { get; set; }
        public string Cpf { get; set; }

        // company
        public string CompanyName { get; set; }
        public string Cnpj { get; set; }
    }
}