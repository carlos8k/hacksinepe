﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;

namespace Neostore.Backend.Domain.Models.Users
{
    public class UserBase : IdentityUser
    {
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public override string Email { get; set; }
        [Required]
        public override string NormalizedEmail { get; set; }

        // general
        public string FacebookId { get; set; }
        public string FacebookToken { get; set; }
        public string ProfilePhotoUrl { get; set; }
        public string DeviceToken { get; set; }

        // control
        public string TimeZone { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime CreateDate { get; set; }
        public IdentityClient IdentityClient { get; set; }
        
        public UserBase()
        {
            CreateDate = DateTime.UtcNow;
        }
    }
}