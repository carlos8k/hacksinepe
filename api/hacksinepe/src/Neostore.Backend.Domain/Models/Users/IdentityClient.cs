﻿namespace Neostore.Backend.Domain.Models.Users
{
    public class IdentityClient
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ClientId { get; set; }
    }
}