﻿namespace Neostore.Backend.Domain.Models.Custom
{
    public class EvaluationReason
    {
        public int ReasonId { get; set; }
        public int EvaluationId { get; set; }

        public virtual Reason Reason { get; set; }
        public virtual Evaluation Evaluation { get; set; }
    }
}