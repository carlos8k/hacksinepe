﻿using Neostore.Backend.Domain.Models.Users;
using System;
using System.Collections.Generic;

namespace Neostore.Backend.Domain.Models.Custom
{
    public class Evaluation
    {
        public int Id { get; set; }
        public int Rating { get; set; }
        
        public DateTime CreateDate { get; set; }

        public Seller Seller { get; set; }
        public Buyer Buyer { get; set; }

        public List<EvaluationReason> EvaluationReasons { get; set; } = new List<EvaluationReason>();
    }
}