﻿using System.Collections.Generic;

namespace Neostore.Backend.Domain.Models.Custom
{
    public class Reason
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsDeleted { get; set; }

        public List<EvaluationReason> EvaluationReasons { get; set; }
    }
}