﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Neostore.Backend.Domain.Models
{
    public class Content
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public string ImageUrl { get; set; }
        public int CategoryId { get; set; }
        [NotMapped]
        public int LikeCount { get; set; }
    }
}