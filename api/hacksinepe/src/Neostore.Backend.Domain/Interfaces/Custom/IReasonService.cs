﻿using Neostore.Backend.Domain.ViewModels.Custom;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Neostore.Backend.Domain.Interfaces.Custom
{
    public interface IReasonService
    {
        Task<IEnumerable<ReasonViewModel>> GetAsync();
        Task<ReasonViewModel> GetAsync(int id);
        Task<ReasonViewModel> CreateAsync(ReasonViewModel model);
        Task UpdateAsync(int id, ReasonViewModel model);
        Task DeleteAsync(int id);
    }
}