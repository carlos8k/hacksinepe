﻿using Neostore.Backend.Domain.ViewModels.Custom;
using System.Threading.Tasks;

namespace Neostore.Backend.Domain.Interfaces.Custom
{
    public interface IEvaluationService
    {
        Task<EvaluationViewModel> CreateAsync(string userId, EvaluationViewModel model);
    }
}