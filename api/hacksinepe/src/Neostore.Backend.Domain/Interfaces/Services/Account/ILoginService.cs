﻿using System.Threading.Tasks;

namespace Neostore.Backend.Domain.Interfaces.Services.Account
{
    public interface ILoginService
    {
        /// <summary>
        /// Sign in a user from sign in manager. 
        /// Throws InvalidOperationException if sign is fails.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        Task SignInAsync(string username, string password);

        /// <summary>
        /// Sign out a user from sign in manager
        /// </summary>
        /// <returns></returns>
        Task SignOutAsync();
    }
}