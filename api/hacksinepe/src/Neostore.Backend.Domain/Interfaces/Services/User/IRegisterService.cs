﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Neostore.Backend.Domain.Interfaces.Services.User
{
    public interface IRegisterService<T, TViewModel>
    {
        Task<IEnumerable<TViewModel>> GetAsync();
        Task<TViewModel> GetAsync(string id);
        Task<TViewModel> CreateAsync(TViewModel model);
        Task UpdateAsync(string id, TViewModel model);
        Task DeleteAsync(string id);
    }
}