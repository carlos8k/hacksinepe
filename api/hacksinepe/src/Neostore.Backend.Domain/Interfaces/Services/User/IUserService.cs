﻿using Neostore.Backend.Domain.ViewModels.Users;
using System.Threading.Tasks;

namespace Neostore.Backend.Domain.Interfaces.Services.User
{
    public interface IUserService
    {
        Task<UserBaseViewModel> GetAsync(string id);
        Task UpdateDeviceTokenAsync(string deviceToken, string userId);
        Task LogoutAsync(string userId);
    }
}