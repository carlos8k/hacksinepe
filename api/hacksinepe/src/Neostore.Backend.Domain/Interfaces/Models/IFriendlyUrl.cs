﻿namespace Neostore.Backend.Domain.Models.Interfaces.Models
{
    public interface IFriendlyUrl
    {
        string Url { get; set; }
    }
}