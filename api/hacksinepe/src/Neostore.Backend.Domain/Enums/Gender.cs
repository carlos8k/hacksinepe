﻿using System.ComponentModel.DataAnnotations;

namespace Neostore.Backend.Domain.Enum
{
    public enum Gender
    {
        [Display(Name = "Masculino")]
        Male = 0,
        [Display(Name = "Feminino")]
        Female = 1
    }
}