﻿using System.ComponentModel.DataAnnotations;

namespace Neostore.Backend.Domain.Enum
{
    public enum OrderStatus
    {
        None = 0,

        [Display(Name = "Aguardando aprovação do vendedor")]
        WaitingApproval = 1,
    
        [Display(Name = "Em processamento")]
        Processing = 2,

        [Display(Name = "Aguardando envio do pedido para marca")]
        WaitingSendToBrand = 3,

        [Display(Name = "Aguardando separação dos produtos")]
        WaitingSeparation = 4,

        [Display(Name = "Um ou mais produtos não estão disponíveis")]
        WaitingProductVerification = 5,

        [Display(Name = "Separado para entrega")]
        Separate = 6,

        [Display(Name = "Entregue")]
        Delivered = 7,

        [Display(Name = "Cancelado pelo vendedor")]
        CanceledBySeller = 8,

        [Display(Name = "Cancelado pelo comprador")]
        CanceledByBuyer = 9
    }
}