﻿using System.ComponentModel.DataAnnotations;

namespace Neostore.Backend.Domain.Enum
{
    public enum PaymentMethod
    {
        [Display(Name = "Nenhum")]
        None = 0,
        [Display(Name = "Cartão de Crédito")]
        CreditCard = 1,
        [Display(Name = "Dinheiro")]
        Money = 2
    }
}
