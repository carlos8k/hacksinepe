﻿using System.ComponentModel.DataAnnotations;

namespace Neostore.Backend.Domain.Enum
{
    public enum PaymentStatus
    {
        None = 0,

        [Display(Name = "Aguardando pré-aprovação")]
        WaitingPreApproval = 1,
        [Display(Name = "Aguardando pagamento")]
        WaitingPayment = 2,
        [Display(Name = "Aguardando estorno")]
        WaitingRefund = 3,

        [Display(Name = "Pré-aprovado")]
        PreApproved = 4,
        [Display(Name = "Pago")]
        Pay = 5,
        [Display(Name = "Estornado")]
        Refunded = 6,

        [Display(Name = "Erro de pré-aprovação")]
        PreApprovalError = 7,
        [Display(Name = "Erro de pagamento")]
        PaymentError = 8,
        [Display(Name = "Erro de estorno")]
        RefundError = 9
    }
}